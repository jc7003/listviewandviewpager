package com.owenchen.viewpagerandlistview;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.Toast;

import com.facebook.ads.AdError;
import com.facebook.ads.NativeAd;
import com.facebook.ads.NativeAdScrollView;
import com.facebook.ads.NativeAdView;
import com.facebook.ads.NativeAdsManager;
import com.owenchen.viewpagerandlistview.Adapter.CustomPagerAdapter;
import com.owenchen.viewpagerandlistview.Adapter.ImageTextListAdapter;
import com.owenchen.viewpagerandlistview.custome.CustomCatalogItem;

import java.util.ArrayList;
import java.util.Vector;

public class MainActivity extends AppCompatActivity implements NativeAdsManager.Listener{

    private final String TAG = "MainActivity";
    private Context mContext;
    private NativeAdsManager mManager;
    private ViewPager mViewPager;

    private ArrayList<ImageTextListAdapter> mALLPageAdapter;
    private ArrayList<ArrayList<CustomCatalogItem>> mALLPageData;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        mContext = this;

        mALLPageData = new ArrayList<ArrayList<CustomCatalogItem>>();
        mALLPageAdapter = new ArrayList<ImageTextListAdapter>();

        initAD();
        initContentList();
    }

    private void initAD(){
        mManager = new NativeAdsManager(this, getString(R.string.ad_key), 5);
        mManager.setListener(this);
        mManager.loadAds(NativeAd.MediaCacheFlag.ALL);
    }

    private void initContentList(){
        ListView listview1 = new ListView(mContext);
        ListView listview2 = new ListView(mContext);
        ListView listview3 = new ListView(mContext);

        Vector<View> pages = new Vector<View>();

        pages.add(listview1);
        pages.add(listview2);
        pages.add(listview3);

        mViewPager = (ViewPager) findViewById(R.id.viewpager);
        CustomPagerAdapter adapter = new CustomPagerAdapter(mContext, pages);
        mViewPager.setAdapter(adapter);

        int[] resource = {  R.layout.list_item_image,
                            R.id.item_text, R.id.item_icon,
                            R.id.item_iconRight,
                            R.id.item_other_view
        };

        mALLPageData.add(getTestData("A", R.mipmap.ic_launcher, 20));
        mALLPageData.add(getTestData("B", R.mipmap.ic_launcher, 15));
        mALLPageData.add(getTestData("C", R.mipmap.ic_launcher, 30));

        for(ArrayList<CustomCatalogItem> arrayData : mALLPageData){
            mALLPageAdapter.add(new ImageTextListAdapter(this, resource, arrayData, mManager));
        }

        listview1.setAdapter(mALLPageAdapter.get(0));
        listview2.setAdapter(mALLPageAdapter.get(1));
        listview3.setAdapter(mALLPageAdapter.get(2));

        mViewPager.setOnPageChangeListener(mSimpleOnPageChangeListener);
    }


    private ViewPager.SimpleOnPageChangeListener mSimpleOnPageChangeListener = new ViewPager.SimpleOnPageChangeListener(){

        @Override
        public void onPageSelected(int i) {
            Log.d(TAG, "onPageSelected position: " + i);
            if(mALLPageData.get(i).get(2).type != CustomCatalogItem.TYPE_AD){
                addAD();
            }
        }

    };

    private ArrayList<CustomCatalogItem> getTestData(String str, int iconid, int count){
        ArrayList<CustomCatalogItem> datas = new ArrayList<CustomCatalogItem>();
        for(int i =0 ; i< count; i++){
            String[] null_info = new String[]{};

            if(i % 7 == 3){
                ImageView imageView = initImageView();
                imageView.setImageResource(R.drawable.one_piece01);
                String[] info = {String.valueOf(R.drawable.one_piece01)};
                CustomCatalogItem data = new CustomCatalogItem(null, 0, CustomCatalogItem.TYPE_IMAGE, info);
                datas.add(data);
            }
            CustomCatalogItem data = new CustomCatalogItem(str + (i+1), iconid, CustomCatalogItem.TYPE_NULL, null_info);
            datas.add(data);
        }

        return datas;
    }

    private ImageView initImageView(){
        ImageView imageView = new ImageView(mContext);
        imageView.setLayoutParams(new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT));
        imageView.setScaleType(ImageView.ScaleType.FIT_CENTER);

        return imageView;
    }

    private String[] getTestData(String str, int count){
        String[] data = new String[count];

        for(int i =0 ; i< count; i++){
            data[i] = str + i;
        }

        return data;
    }

    private void addAD(){
        CustomCatalogItem data = new CustomCatalogItem(null, 0, CustomCatalogItem.TYPE_AD, new String[]{});
        mALLPageData.get(mViewPager.getCurrentItem()).add(2, data);
        mALLPageAdapter.get(mViewPager.getCurrentItem()).notifyDataSetChanged();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onAdsLoaded() {
        Toast.makeText(this, "Ads loaded", Toast.LENGTH_SHORT).show();
        addAD();

    }

    @Override
    public void onAdError(AdError error) {
        Toast.makeText(this, "Ad error: " + error.getErrorMessage(), Toast.LENGTH_SHORT).show();
        Log.d("onAdError", "Ad error: " + error.getErrorMessage());
    }
}
