package com.owenchen.viewpagerandlistview.Adapter;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.facebook.ads.NativeAdScrollView;
import com.facebook.ads.NativeAdView;
import com.facebook.ads.NativeAdsManager;
import com.owenchen.viewpagerandlistview.custome.CustomCatalogItem;

import java.util.ArrayList;

/**
 * Created by owenchen on 2015/9/25.
 */
public class ImageTextListAdapter extends BaseAdapter {

    private LayoutInflater mLayoutInflater;
    private int mLayout;
    private int mTextID, mIconLeft, mIconRight, mOtherLayout;
    private ArrayList<CustomCatalogItem> mItems;
    private Context mContext;

    private NativeAdsManager mManager;

    public ImageTextListAdapter(Context context, int[] resource, ArrayList<CustomCatalogItem> mItems, NativeAdsManager manager) {
        mContext = context;
        mLayoutInflater = (LayoutInflater) context.getSystemService(context.LAYOUT_INFLATER_SERVICE);

        this.mItems = mItems;
        this.mLayout = resource[0];
        mTextID = resource[1];
        mIconLeft = resource[2];
        mIconRight = resource[3];
        mOtherLayout = resource[4];
        mManager = manager;

        Log.d("CatalogAdapter", "mItems.size(): " + mItems.size());
    }

    @Override
    public int getCount() {
        return mItems.size();
    }

    @Override
    public Object getItem(int position) {
        return mItems.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        //自訂類別，表達個別listItem中的view物件集合。
        ViewTag viewTag;

        if(convertView == null) {
            //取得listItem容器 view
            convertView = mLayoutInflater.inflate(mLayout, null);

            //建構listItem內容view
            viewTag = new ViewTag(
                    (ImageView) convertView.findViewById(mIconLeft),
                    (TextView) convertView.findViewById(mTextID),
                    (ImageView) convertView.findViewById(mIconRight),
                    (LinearLayout)convertView.findViewById(mOtherLayout)
            );

            //設置容器內容
            convertView.setTag(viewTag);
        }
        else {
            viewTag = (ViewTag) convertView.getTag();
        }

        switch(mItems.get(position).type){
            case CustomCatalogItem.TYPE_IMAGE:
                ImageView img = new ImageView(mContext);
                img.setScaleType(ImageView.ScaleType.FIT_CENTER);
                img.setImageResource(Integer.parseInt(mItems.get(position).info[0]));
                viewTag.other.removeAllViews();
                viewTag.other.addView(img);
                goneNormal(viewTag);
                break;
            case CustomCatalogItem.TYPE_AD:
                NativeAdScrollView scrollView = new NativeAdScrollView(mContext, mManager,
                        NativeAdView.Type.HEIGHT_100);
                viewTag.other.removeAllViews();
                viewTag.other.addView(scrollView);
                goneNormal(viewTag);
                break;
            default:
                viewTag.title.setText(mItems.get(position).title);
                viewTag.iconL.setImageResource(mItems.get(position).icon);
                showNormal(viewTag);
                viewTag.other.removeAllViews();
                break;
        }

        return convertView;
    }

    private void goneNormal(ViewTag viewTag){
        viewTag.iconL.setVisibility(View.GONE);
        viewTag.title.setVisibility(View.GONE);
        viewTag.iconR.setVisibility(View.GONE);
    }

    private void showNormal(ViewTag viewTag){
        viewTag.iconL.setVisibility(View.VISIBLE);
        viewTag.title.setVisibility(View.VISIBLE);
        viewTag.iconR.setVisibility(View.GONE);
    }

    private class ViewTag{
        TextView title;
        ImageView iconL;
        ImageView iconR;
        LinearLayout other;

        public ViewTag(ImageView icon, TextView title, ImageView iconR, LinearLayout view){
            this.iconL = icon;
            this.title = title;
            this.iconR = iconR;
            this.other = view;
        }
    }
}
