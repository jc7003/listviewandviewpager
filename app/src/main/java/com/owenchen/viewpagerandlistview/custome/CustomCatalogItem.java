package com.owenchen.viewpagerandlistview.custome;

import android.view.View;

/**
 * Created by owenchen on 2015/10/20.
 */
public class CustomCatalogItem {

    public static final int TYPE_NULL = 0;
    public static final int TYPE_IMAGE = 1;
    public static final int TYPE_AD = 2;

    public String title;
    public int icon, type;
    public String[] info;

    public CustomCatalogItem(String title, int icon, int type, String[] info) {
        this.title = title;
        this.icon = icon;
        this.type = type;
        this.info = info;
    }


}
